import requests
import json

r = requests.get('http://server-prototype-assentive-security.mybluemix.net/notifications')

notifications = json.loads(r.text)

print(len(notifications), " notifications found.")
print("")

for n in range(0, len(notifications)-1):
    print(notifications[n]['text'])
    print("\ttime:\t", notifications[n]['time'])



